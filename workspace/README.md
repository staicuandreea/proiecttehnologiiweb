Aplicaţia conţine în momentul de faţă următoarele tabele:
    -ads
    -companies
    -tweets
    -fields
    -ucs
    
Exista o serie de instructiune implementate pentru tabele după cum umează:
    - post:
        - '/add_tweet' adauga o inregistrate in tabela tweets
        - '/add_field' adauga un domeniu (o inregistrare) in tabela fields
        - '/add_company' adauga o noua companie in tabela companies
        
    - get:
        - '/get_ads' returneaza toate inregistrarile din tabela ads
        - '/get_ads_by_id' returneaza companiile in functie de reach-ul introdus
        - '/tweetsId/:tweet_id' returneaza un element de tip tweet pe baza unui id datele
        - '/get-all-fields' returneaza toate domeniile inregistrate
        - '/get_all_companies' returneaza toate comapniile din tabela companies
        - '/companyFollowers/:nr_followers' cauta o companie pe baza numarului de followers specificat si returneaza id-ul acesteia, numele si numarul de followers
        
    - put:    
        - '/companies/:company_id' modifica descrierea unei companii a carei id este specificat
        
    - delete:    
        - '/company/:company_id' sterge din tabela o companie a carei id este specificat