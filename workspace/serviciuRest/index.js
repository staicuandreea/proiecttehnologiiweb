const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(bodyParser.json());

const sequelize = new Sequelize('c9', 'andreeataulea', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const User = sequelize.define('users', {
   name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   username: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   }, 
   password : {
       type: Sequelize.STRING,
       allowNull: false
   }
});

const Company = sequelize.define('companies', {
   company_id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
    },
   name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   description: {
        type: Sequelize.STRING,
        allowNull: false
   },
   location: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   register_date : {
       type: Sequelize.STRING,
       allowNull: false
   },
   nr_followers:{
       type: Sequelize.INTEGER,
       allowNull:true
   }
    
});

const Tweet = sequelize.define('tweets',{
    tweet_id:{
        type:Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        
    },
    tweet_text:{
        type:Sequelize.STRING,
         allowNull: false
    },
    register_time:{
        type: Sequelize.STRING,
         allowNull: false
    },
    company_id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey:true
        
    }
});

const User_Company = sequelize.define('ucs',{
});

User.belongsToMany(Company,{ through: User_Company});

Company.belongsToMany(User, {through: User_Company});

Company.hasMany(Tweet,{
    foreignKey:{
        name:'tweet_id',
        allowNull:  false
    }
});

const Field = sequelize.define('fields',{
     field_code:{
        type:Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
     field_name: {
       type: Sequelize.STRING,
       allowNull: false
   }
});

Company.belongsTo(Field);

sequelize.sync({force: true}).then(()=>{
    console.log('Databases create successfully')
})

app.post('/register', (req, res) =>{
    User.create({
        name: req.body.name,
        surname: req.body.surname,
        username: req.body.username,
        password: req.body.password
    }).then((user) => {
        res.status(200).send("User created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.post('/login', (req, res) => {
   User.findOne({where:{username: req.body.username, password: req.body.password} }).then((result) => {
       res.status(200).send(result)
   }) 
});

app.get('/get-all-users', (req, res)=>{
    User.findAll().then(users_list=>{
      res.status(200).send(users_list)
    })
    
});



app.post('/add_tweet', (req,res)=>{
    Tweet.create(
    {
        tweet_id: req.body.tweet_id,
        tweet_text: req.body.tweet_text,
        register_time: req.body.register_time,
        company_id: req.body.company_id
    
}).then(tweet=>{
    res.status(200).send("Tweet added succesfully");
}, err=>{
    res.status((500).send(err));
});
});

app.get('/tweetsId/:tweet_id', (req,res)=>{
  Tweet.findOne({
       where: {tweet_id:req.body.tweet_id},
       attributes:['tweet_id', ['tweet_text', 'company_id']]
   }).then((company)=>{
         res.status(200).send(company);
   });
});
    
app.post('/add_field',(req,res)=>{
   Field.create({
       field_code: req.body.field_code,
       field_name: req.body.field_name
   }).then(field=>{
       res.status(200).send("Field added successfully");
   }, err=>
   res.status(500).send(err));
});

app.get('/get-all-fields',(req,res)=>{
    Field.findAll().then((fields_list)=>{
        res.status(200).send(fields_list);
    });
});

app.post('/add_company',(req,res)=>{
   Company.create(
       {
           company_id: req.body.company_id,
           name: req.body.name,
           description: req.body.description,
           location: req.body.location,
           register_date: req.body.register_date,
           nr_followers: req.body.nr_followers
           
       }).then(company=>{
           res.status(200).send("Company added succesfully");
       }, err=> {
        res.status(500).send(err);
       })
});


app.put('/companies/:company_id', (req, res) =>{
   Company.update(
       {description: req.body.description },
       {
           where: {company_id: req.body.company_id}
       }
    ).then(company=>
    {res.status(200).send("Updated successfully!");
    },err=> 
    res.status(404).send(err));
    
});

app.delete('/company/:company_id',(req, res)=>{
    Company.destroy({
        where: { company_id:req.params.company_id}
    })
.then(()=>
{res.status(200).send("Deleted");
    
}, err=> res.send(err));
    });



app.get('/get_all_companies',(req,res)=>{
    Company.findAll().then((companies_list)=>{
        res.status(200).send(companies_list);
    });

});

app.get('/companyFollowers/:nr_followers', (req,res)=>{
   Company.findOne({
       where: {nr_followers:req.body.nr_followers},
       attributes:['company_id', ['name', 'nr_followers']]
   }).then((company)=>{
         res.status(200).send(company);
   });
});

app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})

